package pkgSubjects;


import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Random;

import javafx.scene.image.ImageView;
import pkgData.Coordinate;
import pkgMisc.IImageAnimation;
import pkgMisc.SimulationConstants;

public class Person extends Thread implements Comparable<Person>, IImageAnimation {

	public enum HEALTH_STATUS {HEALTHY, SUSPECT, INFECTED, INFECTIVE, DEAD};
	
	protected String name;
	protected Coordinate position = null;
	protected Coordinate oldPosition = null;
	protected static Random rand = new Random(); 
	private int numberOfContacts = 0;
	private int numberOfInfectiveContacts = 0;
	private HEALTH_STATUS healthstatus = HEALTH_STATUS.HEALTHY;
	protected ImageView imgView = null;
	protected boolean isMoving = false;
	
	protected final PropertyChangeSupport supportPropertyChangeCordListener;
	
	private int counter = 0;
	
	public Person(String name, HEALTH_STATUS status) {
		super();
		this.name = name;
		this.position = new Coordinate(0,0);
		this.oldPosition = new Coordinate(0,0);
		this.supportPropertyChangeCordListener = new PropertyChangeSupport(this);
		this.healthstatus = status;
	}
	
	public Person(String name) {
		super();
		this.name = name;
		this.position = new Coordinate(0,0);
		this.oldPosition = new Coordinate(0,0);
		this.supportPropertyChangeCordListener = new PropertyChangeSupport(this);
	}
	
	protected void calculateNewPosition() {
		// find way to make new persons enter the story
		
		int shopLeave = rand.nextInt(100) + 1;
		if (shopLeave < 3 && counter > 10) {
			this.supportPropertyChangeCordListener.firePropertyChange("LeaveShop", this.position, null);
		} else {
			
			int newX = rand.nextInt(SimulationConstants.MAX_LENGTH_ROOM) + 1;
			int newY = rand.nextInt(SimulationConstants.MAX_HIGH_ROOM) + 1;
			
			Coordinate newCord = new Coordinate(newX, newY);
			counter++;
			this.supportPropertyChangeCordListener.firePropertyChange("CoordinateChange", this.position, newCord);
		}

	}
	
	private void simulateLingering() throws InterruptedException {
		
		int value = rand.nextInt((SimulationConstants.MAX_DURATION_STAYING*1000 - SimulationConstants.MIN_DURATION_STAYING*1000) + 1) 
					+ SimulationConstants.MIN_DURATION_STAYING*1000;
		System.out.println(this.getPersonName() + ": sleeping for " + value + "ms");
		Thread.sleep(value);
		System.out.println(this.getPersonName() + ": finished sleeping of " + value + "ms");
	}
	
	@Override
	public void run() {
		try {
			while (true) {
				calculateNewPosition();
				simulateLingering();
			}
		} catch (InterruptedException e) {
			System.out.println(this.name + " " + e.getMessage());
			//e.printStackTrace();
		}
	}

	public String getPersonName() {
		return name;
	}

	public void setPersonName(String name) {
		this.name = name;
	}

	public void setPosition(Coordinate newPosition) {
		this.oldPosition = new Coordinate(this.position);
		this.position = newPosition;
	}
	
	public void addPropertyListener(PropertyChangeListener pcl) {
		this.supportPropertyChangeCordListener.addPropertyChangeListener(pcl);
	}
	
	public void incrementNumberOfContacts() {
		this.numberOfContacts++;
	}
	
	public int getNumberOfContacts() {
		return this.numberOfContacts;
	}
	
	@Override
	public String toString() {
		return name + ",#contacts=" + numberOfContacts + "/" + numberOfInfectiveContacts + " => " + this.healthstatus;
	}

	@Override
	public int compareTo(Person o) {
		
		if (this == o) {
			return 0;
		}
		
		if (this.numberOfContacts > o.numberOfContacts) {
			return -1;
		} else if (this.numberOfContacts == o.numberOfContacts) {
			
			if (this.name.charAt(0) > o.name.charAt(0)) {
				return 1;
			} else if (this.name.charAt(0) > o.name.charAt(0)) {
				return 0;
			} else {
				return -1;
			}
			
		} else {
			return 1;
		}
	}

	public HEALTH_STATUS getHealthStatus() {
		return healthstatus;
	}

	public void setHealthStatus(HEALTH_STATUS healthstatus) {
		this.healthstatus = healthstatus;
	}

	public int getNumberOfInfectiveContacts() {
		return numberOfInfectiveContacts;
	}

	public void incrementNumberOfInfectiveContacts() {
		this.numberOfInfectiveContacts++;
		
		if (this.healthstatus == HEALTH_STATUS.INFECTIVE) {
			
		} else if (this.numberOfInfectiveContacts >= SimulationConstants.MIN_CONTACTS_TILL_INFECTED + SimulationConstants.MIN_CONTACTS_TILL_SUSPTECTED) {
			this.healthstatus = HEALTH_STATUS.INFECTED;
		} else if (this.numberOfInfectiveContacts >= SimulationConstants.MIN_CONTACTS_TILL_SUSPTECTED) {
			this.healthstatus = HEALTH_STATUS.SUSPECT;
		}
	}

	@Override
	public ImageView getImageView() {
		return imgView;
	}

	@Override
	public void setImageView(ImageView iv) {
		this.imgView = iv;
	}

	@Override
	public void setMoving(boolean moving) {
		this.isMoving = moving;
	}

	@Override
	public boolean getMoving() {
		return this.isMoving;
	}

	@Override
	public void checkEnvironment() {
		this.supportPropertyChangeCordListener.firePropertyChange("checkEnvironmentPerson", null, null);
	}
	
	@Override
	public Coordinate getPosition() {
		Coordinate ret = new Coordinate(this.position.getX(), this.position.getY());
		return ret;
	}
	
	@Override
	public Coordinate getOldPosition() {
		Coordinate ret = new Coordinate(this.oldPosition.getX(), this.oldPosition.getY());
		return ret;
	}
	
}
