package pkgSubjects;

import pkgData.Coordinate;
import pkgMisc.SimulationConstants;

public class Security extends Person {

	private int numberInfectionsWitnessed;
	
	public Security(String name, HEALTH_STATUS status) {
		super(name, status);
	}
	
	public Security(String name) {
		super(name);
	}
	
	public void incrementNumberInfectionsWitnessed() {
		this.numberInfectionsWitnessed++;
	}
	
	public int getNumberInfectionsWitnessed() {
		return this.numberInfectionsWitnessed;
	}
	
	@Override
	public String toString() {
		return "Security: " + this.name + " => " + this.numberInfectionsWitnessed;
	}
	
	@Override
	protected void calculateNewPosition() {
		int newX = rand.nextInt(SimulationConstants.MAX_LENGTH_ROOM) + 1;
		int newY = rand.nextInt(SimulationConstants.MAX_HIGH_ROOM) + 1;
		
		Coordinate newCord = new Coordinate(newX, newY);
		this.supportPropertyChangeCordListener.firePropertyChange("CoordinateChange", this.position, newCord);
		
	}
	
}
