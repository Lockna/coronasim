package pkgController;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import pkgData.Coordinate;
import pkgData.DataPool;
import pkgMisc.EventThreadController;
import pkgMisc.EventThreadController.EVENTTHREADTYPE;
import pkgMisc.EventThreadControllerListener;
import pkgMisc.SimulationConstants;
import pkgSubjects.Person;
import pkgSubjects.Person.HEALTH_STATUS;
import pkgSubjects.Security;

public class ThreadController implements PropertyChangeListener {

	private int numberOfThreads;
	private ArrayList<Person> threads;
	private ArrayList<Security> security;
	private ArrayList<Person> dead;
	
	private EventThreadControllerListener listener;
	
	private Coordinate exit = new Coordinate(350,500);
	
	private DataPool pool;
	
	private long startTime;
	private long endTime;
	
	public ThreadController() {
		this.threads = new ArrayList<>();
		this.security = new ArrayList<>();
		this.dead = new ArrayList<>();
		this.pool = new DataPool();
		this.listener = null;
	}
	
	public void generateThreads(int numberOfThreads) {
		
		this.threads.clear();
		this.security.clear();
		this.pool.reset();
		
		this.numberOfThreads = numberOfThreads;
		
		String name;
		
		// Create person thread and create image in gui controller
		for (int i = 0; i < this.numberOfThreads; i++) {
			name = pool.getNextPerson();
			
			Person p = new Person(name);
			System.out.println(p.getPersonName() + " generated");
			p.addPropertyListener(this);
			this.threads.add(p);
			notifyGUI(EVENTTHREADTYPE.CREATE_PERSON, p);
		}
		
		// Calculate percentage of infective persons and set first x % of persons to infective
		for (int i = 0; i < this.threads.size(); i++) {
			if (((double)i/(double)this.threads.size() * 100) < SimulationConstants.getPercentInfectedAtStart()) {
				this.threads.get(i).setHealthStatus(Person.HEALTH_STATUS.INFECTIVE);
				notifyGUI(EVENTTHREADTYPE.CHANGE_IMAGE, this.threads.get(i));
			}
		}
		
		// Calculate how many security guards are needed at the beginning and create them
		int x = (int)(this.numberOfThreads/10);
		
		if (x > 5) x = 5;
		
		for (int i = 0; i < x; i++) {
			name = pool.getNextSecurity();
			Security sec = new Security(name);
			sec.addPropertyListener(this);
			security.add(sec);
			
			notifyGUI(EVENTTHREADTYPE.CREATE_SECURITY, sec);
		}
		
		for (Person p : threads) {
			System.out.println(p);
		}
		
		for (Security s : security) {
			System.out.println(s);
		}
		
		notifyGUI(EVENTTHREADTYPE.ALL_CREATED);
	}
	
	public void startThreads() {
		for (Person p : threads) {
			p.start();
		}
		
		for (Security s : security) {
			s.start();
		}
		
		startTime = System.currentTimeMillis();
		notifyGUI(EVENTTHREADTYPE.ALL_STARTED);
	}
	
	public void stopThreads() {
		for (Person p : threads) {
			p.interrupt();
		}
		
		for (Security s : security) {
			s.interrupt();
		}
		
		endTime = System.currentTimeMillis();
		long timeElapsed = endTime - startTime;
		notifyGUI(String.valueOf(timeElapsed) + "ms elapsed", EVENTTHREADTYPE.ALL_STOPPED);
	}

	@Override
	public synchronized void propertyChange(PropertyChangeEvent evt) {
		Coordinate oldCord = (Coordinate) evt.getOldValue();
		Coordinate newCord = (Coordinate) evt.getNewValue();
		Person source = (Person) evt.getSource();
		
		
		if (evt.getPropertyName().equals("CoordinateChange")) {
			source.setPosition(newCord);
			notifyGUI(EVENTTHREADTYPE.IMAGE_MOVE, source);
		} else if (evt.getPropertyName().equals("checkEnvironmentPerson")) {
			oldCord = source.getOldPosition();
			newCord = source.getPosition();
			
			String msg = (source.getPersonName() + ": " + oldCord.getX() + "/" + oldCord.getY() +  
					" => " + newCord.getX() + "/" + newCord.getY() + "	");
			
			for (Person p : (ArrayList<Person>)threads.clone()) {
				if (p == source || p.getMoving() || source.getMoving() || source.getClass() == Security.class)
					continue;
			
				
				// check if p is within the radius
				double xDiff = newCord.getX() - p.getPosition().getX();
				double yDiff = newCord.getY() - p.getPosition().getY();
				
				double distance = Math.sqrt(xDiff * xDiff + yDiff * yDiff);
				
				if (distance < SimulationConstants.getCurrentDangerousDistance()) {
					msg += " " + p.getPersonName() + ",";
					
					if (source.getHealthStatus() == HEALTH_STATUS.INFECTIVE) {
						p.incrementNumberOfInfectiveContacts();
						notifyGUI(EVENTTHREADTYPE.CHANGE_IMAGE, p);
						checkForSecurity(source, p);
					}				
					
					if (p.getHealthStatus() == HEALTH_STATUS.INFECTIVE) {
						source.incrementNumberOfInfectiveContacts();
						notifyGUI(EVENTTHREADTYPE.CHANGE_IMAGE, source);
						checkForSecurity(p, source);
					}
					
					p.incrementNumberOfContacts();
					source.incrementNumberOfContacts();				
				}
			}
			
			notifyGUI(msg, EVENTTHREADTYPE.LOG);
			System.out.println(msg);
			
		} else if (evt.getPropertyName().equals("LeaveShop")) {
			
			source.setPosition(exit);
			
			notifyGUI(EVENTTHREADTYPE.LEAVE_STORE, source);
			source.interrupt();
			
			this.threads.remove(source);
		}
	}
	
	private synchronized void checkForSecurity(Person p1, Person p2) {
		
		for (Security s : security) {
			
			if (s.getMoving())
				continue;
			
			if (p1.getHealthStatus() == p2.getHealthStatus())
				continue;
			
			Coordinate middlePoint = new Coordinate((int)((p1.getPosition().getX() + p2.getPosition().getX()) / 2),
													(int)((p1.getPosition().getY() + p2.getPosition().getY()) / 2));
			
			double xDiff = middlePoint.getX() - s.getPosition().getX();
			double yDiff = middlePoint.getY() - s.getPosition().getY();
			
			double distance = Math.sqrt(xDiff * xDiff + yDiff * yDiff);
			
			if (distance < SimulationConstants.getSecurityRange()) {
				p1.setHealthStatus(HEALTH_STATUS.DEAD);
				p2.setHealthStatus(HEALTH_STATUS.DEAD);
				
				p1.interrupt();
				p2.interrupt();
				
				p1.setPosition(exit);
				p2.setPosition(exit);
				
				dead.add(p1);
				dead.add(p2);
				
				threads.remove(p1);
				threads.remove(p2);
				
				notifyGUI(EVENTTHREADTYPE.CHANGE_IMAGE, p1);
				notifyGUI(EVENTTHREADTYPE.CHANGE_IMAGE, p2);
				
			}
		}
	}
	
	private void notifyGUI(String msg, EVENTTHREADTYPE type) {
		listener.onEventThreadController(new EventThreadController(this, msg, type));
	}
	
	private void notifyGUI(EVENTTHREADTYPE type) {
		listener.onEventThreadController(new EventThreadController(this, type));
	}
	
	private void notifyGUI(EVENTTHREADTYPE type, Person person) {
		listener.onEventThreadController(new EventThreadController(this, type, person));
	}
	
	public void setListener(EventThreadControllerListener listener) {
		this.listener = listener; 
	}
	
	public ArrayList<Person> getPersons() {
		return this.threads;
	}
	
	public ArrayList<Person> getDead() {
		return this.dead;
	}
	
}
