package pkgController;


import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ResourceBundle;

import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.ArcTo;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.HLineTo;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.VLineTo;
import javafx.stage.Stage;
import javafx.util.Duration;
import pkgMisc.CellListViewContacts;
import pkgMisc.CellListViewHealth;
import pkgMisc.ComparatorHealthStatus;
import pkgMisc.EventThreadController;
import pkgMisc.EventThreadController.EVENTTHREADTYPE;
import pkgMisc.EventThreadControllerListener;
import pkgMisc.IImageAnimation;
import pkgMisc.SimulationConstants;
import pkgSubjects.Person;
import pkgSubjects.Person.HEALTH_STATUS;

public class MainController implements Initializable, EventThreadControllerListener {
	@FXML
    private Button btnGenerate;

    @FXML
    private Button btnLog;

    @FXML
    private Button btnStart;

    @FXML
    private Button btnStop;
    
    @FXML
    private Button btnCallAmbulance;

    @FXML
    private TextField inputPersons;
    
    @FXML
    private TextField percentInfective;

    @FXML
    private Label lblMessage;

    @FXML
    private ListView<String> eventLog;
    
    @FXML
    private ListView<Person> lstHealthStatus;
    
    @FXML
    private Slider distanceSlider;
    
    @FXML
    private Slider securityRange;
    
    @FXML
    private Pane shopPane;
    
    @FXML
    void onClickGenerate(ActionEvent event) {
    	try {
    		eventLog.getItems().clear();
    		Platform.runLater(() -> shopPane.getChildren().clear());
    		ImageView bg = new ImageView(background);
    		
    		Platform.runLater(() -> shopPane.getChildren().add(bg));
    		SimulationConstants.setPercentInfectedAtStart(Integer.parseInt(percentInfective.getText()));
    		tc.generateThreads(Integer.parseInt(inputPersons.getText()));
    	} catch (Exception e) {
    		lblMessage.setText(e.getMessage());
    	}
    }

    private void displayLog() throws Exception {
    	
    	ArrayList<Person> persons = tc.getPersons();
    	
    	persons.sort(new ComparatorHealthStatus());
    	lstHealthStatus.getItems().clear();
    	lstHealthStatus.getItems().addAll(persons);
    }
    
    @FXML
    void onClickStart(ActionEvent event) {
    	try {
    		tc.startThreads();
    	} catch (IllegalThreadStateException e) {
    		lblMessage.setText("threads already started");
    		//e.printStackTrace();
    	}
    }
    
    @FXML
    void onClickStop(ActionEvent event) {
    	tc.stopThreads();
    }
    
    @FXML 
    void onClickLog(ActionEvent event) {
    	try {
			displayLog();
			lblMessage.setText("log displayed...");
		} catch (Exception e) {
			lblMessage.setText("error: + " + e.getMessage());
			//e.printStackTrace();
		}
    }
    
    @FXML
    void onCallAmbulance(ActionEvent event) {	
    	
    	if (this.ambAnim) {
    		return;
    	}
    	
    	ambAnim = true;
    	
    	ArrayList<Person> dead = tc.getDead();
    	
    	Pane pane = (Pane)myStage.getScene().getRoot();
    	
    	ImageView ivAmbL = new ImageView(imgAmbL);
    	
    	pane.getChildren().add(ivAmbL);
    	
    	PathTransition pathTransition = new PathTransition();
    	pathTransition.setDuration(Duration.millis(SimulationConstants.ANIMATION_DURATION));
		
		pathTransition.setNode(ivAmbL);
    	Path path = new Path();
    	
    	path.getElements().add(new MoveTo(1500, 675));
		path.getElements().add(new HLineTo(850));
		pathTransition.setPath(path);
		
		pathTransition.setOnFinished(new EventHandler<ActionEvent>() {
		    @Override
		    public void handle(ActionEvent event) {
		       
		    	for (Person p : dead) {
		    		onEventThreadController(new EventThreadController(this, EVENTTHREADTYPE.LEAVE_STORE, p));
		    	}
		    	
		    	Path path = new Path();
		    	path.getElements().add(new MoveTo(850, 675));
				path.getElements().add(new HLineTo(550));
				path.getElements().add(new VLineTo(75));
				path.getElements().add(new HLineTo(0));
				pathTransition.setPath(path);
				
				pathTransition.setOnFinished(new EventHandler <ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						pathTransition.stop();
						pane.getChildren().remove(ivAmbL);
						ambAnim = false;
					}
					
				});
				
				Timeline playtime = new Timeline(
					    new KeyFrame(Duration.seconds(0), eventL -> pathTransition.pause()),
					    new KeyFrame(Duration.seconds(2), eventL -> pathTransition.play())
					);
				playtime.play();
		    }
		});
		
		pathTransition.play();
    }

	public static void setStage(Stage stage) {
		myStage = stage;
	}
    
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {
			tc = new ThreadController();
			tc.setListener(this);
			eventLog.setCellFactory(new CellListViewContacts());
			lstHealthStatus.setCellFactory(new CellListViewHealth());
			
			distanceSlider.setMin(15);
			distanceSlider.setMax(35);
			distanceSlider.setValue(15);
			distanceSlider.setShowTickMarks(true);
			distanceSlider.setBlockIncrement(5);
			distanceSlider.setMajorTickUnit(5);
			distanceSlider.setMinorTickCount(0);
			distanceSlider.setShowTickLabels(true);
			distanceSlider.setSnapToTicks(true);
			
			securityRange.setMin(50);
			securityRange.setMax(150);
			securityRange.setValue(50);
			securityRange.setShowTickMarks(true);
			securityRange.setBlockIncrement(5);
			securityRange.setMajorTickUnit(5);
			securityRange.setMinorTickCount(0);
			securityRange.setShowTickLabels(true);
			securityRange.setSnapToTicks(true);
			
			SimulationConstants.setCurrentDangerousDistance(15);
			
			distanceSlider.addEventHandler(MouseEvent.MOUSE_DRAGGED, this::changeDangerousDistance);
			securityRange.addEventFilter(MouseEvent.MOUSE_DRAGGED, this::changeSecurityRange);
			
			imgHealthy = new Image(SimulationConstants.FILE_PERSON_HEALTHY);
    		imgInfected = new Image(SimulationConstants.FILE_PERSON_INFECTED);
    		imgInfective = new Image(SimulationConstants.FILE_PERSON_INFECTIVE);
    		imgSuspect = new Image(SimulationConstants.FILE_PERSON_SUS);
    		imgDead = new Image(SimulationConstants.FILE_PERSON_DEAD);
    		imgSec = new Image(SimulationConstants.FILE_SECURITY);
    		imgAmb = new Image(SimulationConstants.FILE_AMBULANCE);
    		imgAmbL = new Image(SimulationConstants.FILE_AMBULANCE_LIGHT);
    		background = new Image(SimulationConstants.FILE_BACKGROUND, SimulationConstants.MAX_LENGTH_ROOM, SimulationConstants.MAX_HIGH_ROOM, false, true);
    		
    		shopPane.getChildren().add(new ImageView(background));
    		
		} catch (Exception e) {
			lblMessage.setText("error: " + e.getMessage());
			//e.printStackTrace();
		}
	}
	
	// non gui attributes
	private ThreadController tc = null;
	private static Stage myStage = null;
	private boolean ambAnim = false;
	private Image imgHealthy;
    private Image imgInfected;
    private Image imgInfective;
    private Image imgSuspect;
    private Image imgDead;
    private Image imgSec;
    private Image imgAmb;
    private Image imgAmbL;
    private Image background;
	
	@Override
	public void onEventThreadController(EventThreadController event) {
		
		// make branch for store leaving with animation with coordinates of the store entry
		// branch for people dying (making their picture black)
		// branch for getting ambulance car at the entrance and somehow deliver all dead people to the ambulance car which leaves
		
		if (event.getType() == EVENTTHREADTYPE.CREATE_PERSON) {
			
			IImageAnimation ia = event.getAnimation();
			ImageView img = new ImageView();
			
			img.setFitHeight(SimulationConstants.HEIGHT);
			img.setFitWidth(SimulationConstants.WIDTH);
			img.setX(ia.getPosition().getX());
			img.setY(ia.getPosition().getY());
			img.setImage(getImageFromHealth(ia.getHealthStatus()));
			img.setVisible(true);
			ia.setImageView(img);
			
			Platform.runLater(() -> shopPane.getChildren().add(img));
		} else if (event.getType() == EVENTTHREADTYPE.CREATE_SECURITY) {
			
			IImageAnimation ia = event.getAnimation();
			ImageView img = new ImageView();
			
			img.setFitHeight(SimulationConstants.HEIGHT);
			img.setFitWidth(SimulationConstants.WIDTH);
			img.setX(ia.getPosition().getX());
			img.setY(ia.getPosition().getY());
			img.setImage(imgSec);
			img.setVisible(true);
			ia.setImageView(img);
			Platform.runLater(() -> shopPane.getChildren().add(img));
		} else if (event.getType() == EVENTTHREADTYPE.IMAGE_MOVE) {
			IImageAnimation ia = event.getAnimation();
			ImageView img = ia.getImageView();
			
			Path path = new Path();
			path.getElements().add(new MoveTo(ia.getOldPosition().getX(), ia.getOldPosition().getY()));
			path.getElements().add(new LineTo(ia.getPosition().getX(), ia.getPosition().getY()));

			PathTransition pathTransition = new PathTransition();
			pathTransition.setDuration(Duration.millis(SimulationConstants.ANIMATION_DURATION));
			pathTransition.setPath(path);
			pathTransition.setNode(img);
			
			ia.setMoving(true);
			pathTransition.setOnFinished(new EventHandler<ActionEvent>() {
			    @Override
			    public void handle(ActionEvent event) {
			        ia.setMoving(false);
			        ia.checkEnvironment();
			    }
			});
			
			pathTransition.play();
		} else if (event.getType() == EVENTTHREADTYPE.CHANGE_IMAGE) {
			IImageAnimation ia = event.getAnimation();
			ImageView imgView = ia.getImageView();
			imgView.setImage(getImageFromHealth(ia.getHealthStatus()));
			
		} else if (event.getType() == EVENTTHREADTYPE.LOG) {
			Platform.runLater(() -> eventLog.getItems().add(event.getMessage()));
		} else if (event.getType() == EVENTTHREADTYPE.LEAVE_STORE) {
			
			IImageAnimation ia = event.getAnimation();
			ImageView img = ia.getImageView();
			
			
			Path path = new Path();
			path.getElements().add(new MoveTo(ia.getOldPosition().getX(), ia.getOldPosition().getY()));
			path.getElements().add(new LineTo(ia.getPosition().getX(), ia.getPosition().getY()));

			PathTransition pathTransition = new PathTransition();
			pathTransition.setDuration(Duration.millis(SimulationConstants.ANIMATION_DURATION));
			pathTransition.setPath(path);
			pathTransition.setNode(img);
			
			pathTransition.setCycleCount(1);
			pathTransition.setAutoReverse(false);
			
			ia.setMoving(true);
			
			pathTransition.setOnFinished(new EventHandler<ActionEvent>() {
			    @Override
			    public void handle(ActionEvent event) {
			        ia.setMoving(false);
			        Platform.runLater(() -> shopPane.getChildren().remove(img));
			    }
			});
			pathTransition.play();
		} else {
			Platform.runLater(() -> lblMessage.setText("threadcontroller: " + event.getType()));
		}
	}
	
	private void changeDangerousDistance(MouseEvent e) {
		SimulationConstants.setCurrentDangerousDistance((int)distanceSlider.getValue());
	}
	
	private void changeSecurityRange(MouseEvent e) {
		SimulationConstants.setSecurityRange((int)securityRange.getValue());
	}
	
	private Image getImageFromHealth(HEALTH_STATUS status) {
		Image ret = null;
		
		if (status  == HEALTH_STATUS.HEALTHY) {
			ret = imgHealthy;
		} else if (status == HEALTH_STATUS.INFECTED) {
			ret = imgInfected;
		} else if (status == HEALTH_STATUS.INFECTIVE) {
			ret = imgInfective;
		} else if (status == HEALTH_STATUS.SUSPECT) {
			ret = imgSuspect;
		} else if (status == HEALTH_STATUS.DEAD) {
			ret = imgAmb;
		}
		
		return ret;
	}
}
