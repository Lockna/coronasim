package pkgMisc;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class CellListViewContacts implements Callback<ListView<String>, ListCell<String>> {

	@Override
	public ListCell<String> call(ListView<String> param) {
		
		return new ListCell<>() {
			
			@Override
			public void updateItem(String msg, boolean empty) {
				super.updateItem(msg, empty);
				
				setText(msg);
				
				if (msg != null && msg.contains(",")) {
					setStyle("-fx-background-color: lightgreen;");
				} else {
					setStyle("-fx-background-color: white");
				}
			}
			
		};
		
	}

}
