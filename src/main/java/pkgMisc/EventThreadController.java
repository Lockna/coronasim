package pkgMisc;

import java.util.EventObject;

import pkgSubjects.Person;

public class EventThreadController extends EventObject {

	private String message;
	private EVENTTHREADTYPE type;
	private IImageAnimation ia;
	
	private Person p1;
	private Person p2;
	
	public EventThreadController(Object source) {
		this(source, "unknown error occursed", EVENTTHREADTYPE.UNDEFINED);
		this.message = "";
	}
	
	public enum EVENTTHREADTYPE {
		ALL_STARTED,
		ALL_STOPPED,
		ALL_CREATED,
		CREATE_PERSON,
		CREATE_SECURITY,
		LEAVE_STORE,
		IMAGE_MOVE,
		LOG,
		CHANGE_IMAGE,
		ERROR,
		UNDEFINED
	};
	
	public EventThreadController(Object source, String message, EVENTTHREADTYPE type) {
		super(source);
		this.message = message;
		this.type = type;
	}
	
	public EventThreadController(Object source, EVENTTHREADTYPE type) {
		super(source);
		this.type = type;
	}
	
	public EventThreadController(Object source, EVENTTHREADTYPE type, IImageAnimation ia) {
		super(source);
		this.type = type;
		this.ia = ia;
	}
	
	public EventThreadController(Object source, Person p1, Person p2) {
		super(source);
		this.p1 = p1;
		this.p2 = p2;
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public EVENTTHREADTYPE getType() {
		return this.type;
	}
	
	public IImageAnimation getAnimation() {
		return this.ia;
	}
	
}
