package pkgMisc;

import java.util.EventListener;

public interface EventThreadControllerListener extends EventListener {
	void onEventThreadController(EventThreadController event);
}
