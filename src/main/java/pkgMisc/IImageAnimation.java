package pkgMisc;

import javafx.scene.image.ImageView;
import pkgData.Coordinate;
import pkgSubjects.Person.HEALTH_STATUS;

public interface IImageAnimation {
	ImageView getImageView();
	void setImageView(ImageView iv);
	Coordinate getOldPosition();
	Coordinate getPosition();
	HEALTH_STATUS getHealthStatus();
	void setMoving(boolean moving);
	boolean getMoving();
	void checkEnvironment();
}
