package pkgMisc;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import pkgSubjects.Person;

public class CellListViewHealth implements Callback<ListView<Person>, ListCell<Person>> {

	@Override
	public ListCell<Person> call(ListView<Person> param) {
		
		return new ListCell<>() {
			
			@Override
			public void updateItem(Person person, boolean empty) {
				if (person == null) {
					return;
				}
				super.updateItem(person, empty);
				
				setText(person.toString());
				
				if (person.getHealthStatus() == Person.HEALTH_STATUS.INFECTIVE) {
					setStyle("-fx-background-color: red;");
				} else if (person.getHealthStatus() == Person.HEALTH_STATUS.INFECTED){
					setStyle("-fx-background-color: orange;");
				} else if (person.getHealthStatus() == Person.HEALTH_STATUS.SUSPECT){
					setStyle("-fx-background-color: yellow;");
				} else if (person.getHealthStatus() == Person.HEALTH_STATUS.HEALTHY){
					setStyle("-fx-background-color: palegreen;");
				}
			}
			
		};
		
	}

}
