package pkgMisc;

public class SimulationConstants {
	public static final int MAX_LENGTH_ROOM = 700;
	public static final int MAX_HIGH_ROOM = 500;
	public static final int DANGEROUS_DISTANCE = 20;
	public static final int MAX_DURATION_STAYING = 6;
	public static final int MIN_DURATION_STAYING = 3;
	public static final int MIN_CONTACTS_TILL_SUSPTECTED = 1; // number of contacts necessary to get health => suspected
	public static final int MIN_CONTACTS_TILL_INFECTED = 1; // number of contacts necessary to get suspected => infected
	public static final String LOGFILE = "log.txt";
	
	// animation constants
	public static final int X_COO_SHOP = 600;
	public static final int Y_COO_SHOP = 140;
	public static final int HEIGHT = 30;
	public static final int WIDTH = 30;
	public static final long ANIMATION_DURATION = 2000;
	public static final String FILE_PERSON_HEALTHY = "/pkgController/images/healthy.png";
	public static final String FILE_PERSON_INFECTED = "/pkgController/images/infected.png";
	public static final String FILE_PERSON_INFECTIVE = "/pkgController/images/infective.png";
	public static final String FILE_PERSON_SUS = "/pkgController/images/suspect.png";
	public static final String FILE_PERSON_DEAD = "/pkgController/images/Dead.png";
	public static final String FILE_SECURITY = "/pkgController/images/Security.png";
	public static final String FILE_AMBULANCE = "/pkgController/images/AMB.png";
	public static final String FILE_AMBULANCE_LIGHT = "/pkgController/images/ambulance_light.gif";
	public static final String FILE_BACKGROUND = "/pkgController/images/background.png";
	
	private static int securityRange = 150;
	
	private static int currentDangerousDistance = 15; //m
	private static int percentInfectedAtStart = 0; //%
	
	public static int getCurrentDangerousDistance() {
		return currentDangerousDistance;
	}
	
	public static void setCurrentDangerousDistance(int currentDangerousDistance) {
		SimulationConstants.currentDangerousDistance = currentDangerousDistance;
	}
	
	public static int getPercentInfectedAtStart() {
		return percentInfectedAtStart;
	}
	
	public static void setPercentInfectedAtStart(int percentInfectedAtStart) {
		SimulationConstants.percentInfectedAtStart = percentInfectedAtStart;
	}
	
	public static int getSecurityRange() {
		return securityRange;
	}
	
	public static void setSecurityRange(int x) {
		SimulationConstants.securityRange = x;
	}
	
}
