package pkgMisc;

import java.util.Comparator;

import pkgSubjects.Person;

public class ComparatorHealthStatus implements Comparator<Person> {

	@Override
	public int compare(Person o1, Person o2) {
		
		if (o1.getHealthStatus().ordinal() > o2.getHealthStatus().ordinal()) {
			return -1;
		} else if (o1.getHealthStatus().ordinal() == o2.getHealthStatus().ordinal()) {
			
			if (o1.getPersonName().charAt(0) > o2.getPersonName().charAt(0)) {
				return 1;
			} else if (o1.getPersonName().charAt(0) > o2.getPersonName().charAt(0)) {
				return 0;
			} else {
				return -1;
			}
			
		} else {
			return 1;
		}
	}

}
