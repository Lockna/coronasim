package pkgMain;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pkgController.MainController;

public class MainApp extends Application {
	    @Override
	    public void start(Stage primaryStage) throws Exception{
	        Parent root = FXMLLoader.load(getClass().getResource("/pkgController/MainGui.fxml"));
	        primaryStage.setTitle("Shops");
	        primaryStage.setScene(new Scene(root));
	        MainController.setStage(primaryStage);
	        primaryStage.show();
	    }
	    public static void main(String[] args) {
	        launch(args);
	    }
}
