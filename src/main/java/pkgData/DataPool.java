package pkgData;

import java.util.ArrayList;

public class DataPool {
	
	private ArrayList<String> collPersons = new ArrayList<>();
	private ArrayList<String> collSecurity = new ArrayList<>();
	
	private int indexPerson = 0;
	private int indexSecurity = 0;
	
	public DataPool() {
		collPersons.add("Hannah");
		collPersons.add("Raphael Obkircher");
		collPersons.add("Mathias");		
		collPersons.add("Theo Ebner");
		collPersons.add("Fabian");
		collPersons.add("Adrian");
		collPersons.add("Laetitia");
		collPersons.add("Stefan");
		collPersons.add("Johannes");
		collPersons.add("Neil");
		collPersons.add("Valentin");
		collPersons.add("Sir Gabel");
		collPersons.add("Dean");
		collPersons.add("Julian");
		collPersons.add("Marcel");
		collPersons.add("Lisa");
		collPersons.add("Kathi");
		collPersons.add("Selina");
		collPersons.add("Lukas");
		collPersons.add("Markus");
		collPersons.add("Carolina");
		collPersons.add("Marco");
		collPersons.add("Matthias");
		collPersons.add("Leonie");
		collPersons.add("Hanna");
		collPersons.add("Clara");
		collPersons.add("Sophia");
		collPersons.add("Brian O'Connor");
		collPersons.add("Sean");
		collPersons.add("Twinkie");
		collPersons.add("Grazie");
		collPersons.add("noname");
		collPersons.add("Tyrion");
		collPersons.add("Eddard");
		collPersons.add("Chiara");
		
		collSecurity.add("Gertrude");
		collSecurity.add("Owen");
		collSecurity.add("Jamie lannister");
		collSecurity.add("Iron Man");
		collSecurity.add("Captain America");
	}
	
	public String getNextPerson() {
		String retPerson = collPersons.get(indexPerson++);
		return retPerson;
	}
	
	public String getNextSecurity() {
		String retSecurity = collSecurity.get(indexSecurity++);
		return retSecurity;
	}
	
	public void reset() {
		this.indexPerson = 0;
		this.indexSecurity = 0;
	}
	
}
